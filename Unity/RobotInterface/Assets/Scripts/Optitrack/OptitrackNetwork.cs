﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.Serialization;

namespace Optitrack
{
    [RequireComponent(typeof(FrankaPanda))]
    public class OptitrackNetwork : MonoBehaviour
    {
        private FrankaPanda panda;
        private Socket sendingSocket;
        private Socket receivingNetworkSocket;
        private IPAddress broadcast;
        private IPEndPoint ep;
        private byte[] buffer = new byte[64];
        public String ip;
        public int receivingPort = 513;
        public int sendingPort = 514;
        private bool streaming = true;
        private Thread receiverThread;
        
        private CartesianPose cartesianPose;

        public void SendPosition()
        {
            byte[] sendbuf = Converter.CreateBytePackageFromVector(cartesianPose.position);
            sendingSocket.SendTo(sendbuf, ep);
        }

        private void GetEndeffectorPositionFromFranka()
        {
            cartesianPose.position = panda.GetPosition();
            SendPosition();
        }

        private void OnDestroy()
        {
            sendingSocket.Close();
            receivingNetworkSocket.Close();
        }

  

        public void StartUpdateLoop()
        {
            do
            {
                buffer = new byte[64];
                
                receivingNetworkSocket.Receive(buffer);
                switch (buffer[0])
                {
                    case 1:
                       
                        
                        cartesianPose.position = Converter.ConvertFloatToPosition(buffer);
                        Debug.Log("Send Panda to:" + cartesianPose.position);
                        try
                        {
                            panda.MoveToPoint(cartesianPose, 0.5);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        break;
                    case 2:
                        //orientation update
                        throw new NotImplementedException();
                        break;
                    case 99:
                        //end streaming
                        streaming = false;
                        break;
                }

            } while (streaming);
        }
        

        private void Start()
        {
            
            cartesianPose = CartesianPose.initialPose; 
            panda = GetComponent<FrankaPanda>();
            Debug.Log("Initializing Optitrack network...");
           
            
            sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            broadcast = IPAddress.Parse(ip);
            ep = new IPEndPoint(broadcast, sendingPort);
            
            receivingNetworkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
                ProtocolType.Udp);
            IPAddress ipAddress = IPAddress.Parse(ip);
            var remoteEP = new IPEndPoint(IPAddress.Any, receivingPort); //solve via IPAdress.Any (no 'static' connection)
            receivingNetworkSocket.Bind(remoteEP); 
            
            receiverThread = new Thread(StartUpdateLoop);
            receiverThread.Start();
            
        }

        private int i = 0;

        private void Update()
        {
            if (i >= 10)
            {
                GetEndeffectorPositionFromFranka();
                SendPosition();
                i = 0;
            }
            else
            {
                i++;

            }



        }

        
    }
}