﻿using UnityEngine;
using XNode;


public class ConditionNode : ActionNode
{
    [Input] public Empty enter;
    [Output] public Empty exitA;
    [Output] public Empty exitB;
    [Input] public bool condition;

    public override void MoveNext()
    {
        MotionGraph fmGraph = graph as MotionGraph;

        if (fmGraph.current != this)
        {
            Debug.LogWarning("Node isn't active");
            return;
        }

        NodePort exitPort;

        if (condition)
        {
            exitPort = GetOutputPort("exitA");
        }
        else
        {
            exitPort = GetOutputPort("exitB");
        }

        if (!exitPort.IsConnected)
        {
            Debug.LogWarning("Node isn't connected");
            return;
        }

        ActionNode node = exitPort.Connection.node as ActionNode;
        node.OnEnter();
    }

    public override void OnEnter()
    {
        MotionGraph fmGraph = graph as MotionGraph;
        fmGraph.current = this;
        Execute();
    }

    public override void Execute()
    {
        //FrankaPanda.GetInstance().LocalRotation(roll, pitch, yaw, duration);
    }

    public override void Preview()
    {
        throw new System.NotImplementedException();
    }
}