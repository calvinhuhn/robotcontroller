﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;

[RequireComponent(typeof(FrankaPanda))]
public class TransmitCartesianPoint : MonoBehaviour
{
    #region debug
    public bool debugEnabled;
    #endregion
    
    #region constants

    IPEndPoint endPoint;
    #endregion
    [SerializeField]
    public FrankaPanda gripper;
    [SerializeField]
    public Robot robot;


    private Socket receivingNetworkSocket;

    // Start is called before the first frame update
    void Start()
    {
        InitUDPSocket();
        //gripper = GetComponent<Gripper>();
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            SendTransformation(); 
        }
        
    }

    public void InitUDPSocket() {
        if (debugEnabled == true) Debug.Log("Init Socket with: UDP");
        receivingNetworkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
        ProtocolType.Udp);

        IPAddress serverAddr = IPAddress.Parse(robot.settings.ReceiverIp);
     
        endPoint = new IPEndPoint(serverAddr, robot.settings.ReceiverPort);



    }

    public void SendTransformation() {

    }


  
}
