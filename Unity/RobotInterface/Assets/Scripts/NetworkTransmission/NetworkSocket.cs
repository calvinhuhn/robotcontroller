﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;


public class NetworkSocket
{
    Socket s;

    IPAddress broadcast;

    private NetworkSocket()
    {
        s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
    }

    public NetworkSocket(String ip) : this()
    {        
        broadcast = IPAddress.Parse(ip);
    }


    public void SendPosition(Vector3 position)
    {
        byte[] sendbuf = CreateBytePackage(position); 
        IPEndPoint ep = new IPEndPoint(broadcast, 514);
    
        s.SendTo(sendbuf, ep);
        
    }

    public byte[] CreateBytePackage(Vector3 position)
    {
        byte[] outgoingArray = new byte[24];
        byte[] x = BitConverter.GetBytes((double)position.x);
        byte[] y = BitConverter.GetBytes((double)position.y);
        byte[] z = BitConverter.GetBytes((double)position.z);
        for (int i = 0; i < 8; i++) {
            outgoingArray[i] = x[i];
            outgoingArray[i + 8] = y[i];
            outgoingArray[i + 16] = z[i];
        }
      
        return outgoingArray;
    }



}