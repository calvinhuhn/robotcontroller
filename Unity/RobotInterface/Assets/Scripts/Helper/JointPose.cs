﻿using UnityEngine;

[System.Serializable]
public struct JointPose
{
    private static readonly JointPose initPose = new JointPose(0, -Mathf.PI / 4, 0, -3 * Mathf.PI / 4, 0, Mathf.PI / 2, Mathf.PI / 4);
    
    [Range(-2.897f, 2.897f)]
    public double joint_0;
    [Range(-1.762f, 1.762f)]
    public double joint_1;
    [Range(-2.897f, 2.897f)]
    public double joint_2;
    [Range(-0.069f, -3.071f)]
    public double joint_3;
    [Range(-2.897f, 2.897f)]
    public double joint_4;
    [Range(-0.017f, 3.752f)]
    public double joint_5;
    [Range(-2.897f, 2.897f)]
    public double joint_6;

    public override string ToString()
    {
        return "{0: " +  joint_0 + " , 1: " + joint_1 + " , 2: " + joint_2 + " , 3: " + joint_3 + " , 4: " + joint_4 + " , 4: " + joint_5 + " , : " + joint_6 + "}";
    }

    public JointPose(double joint_0, double joint_1, double joint_2,double joint_3,double joint_4,double joint_5, double joint_6)
    {
        this.joint_0 = joint_0;
        this.joint_1 = joint_1;
        this.joint_2 = joint_2;
        this.joint_3 = joint_3;
        this.joint_4 = joint_4;
        this.joint_5 = joint_5;
        this.joint_6 = joint_6;
    }
    

    public static JointPose initialPose
    {
        get
        {
            return JointPose.initPose;
        }
    }
}