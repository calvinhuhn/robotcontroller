﻿using UnityEngine;

[System.Serializable]
public struct CartesianPose
{
    public Vector3 position;
    public Vector3 orientation;
    
    private static readonly CartesianPose initPose = new CartesianPose(new Vector3(0.0f, 0.5f, 0.3f), Vector3.zero);
    private static readonly CartesianPose pickup = new CartesianPose(new Vector3(0.0f, -0.02f, 0.4f), Vector3.zero);

    public CartesianPose(Vector3 position, Vector3 orientation)
    {
        this.position = position;
        this.orientation = orientation;
    }

    public static CartesianPose initialPose
    {
        get
        {
            return CartesianPose.initPose;
        }
    }    
    
    public static CartesianPose PickupPose
    {
        get
        {
            return CartesianPose.pickup;
        }
    }    
    
 

    public CartesianPose Copy()
    {
        return new CartesianPose(new Vector3(position.x, position.y, position.z), new Vector3(orientation.x, orientation.y, orientation.z));
    }

}