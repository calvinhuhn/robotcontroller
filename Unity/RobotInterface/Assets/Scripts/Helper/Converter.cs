﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public static class Converter
{
    private static int PACKAGE_TYPE_SIZE = 1;
    public static byte[] CreateActionPackage(ActionCode actionCode)
    {
        byte[] outgoingArray = new byte[2]; //7 joints, each has a float value with 4 bytes (28 bytes total), one typeByte and one action type byte
        outgoingArray[0] = 4; //4 is package type for actions. See the Documentation for package types
        if (actionCode == ActionCode.CONNECT)
        {
            outgoingArray[1] = 0; //0 -> connect
        }
        else
        {
            outgoingArray[1] = Byte.MaxValue;
        }

        
        return outgoingArray;
    }

    public static JointPose UpdateJoints(byte[] buffer, ref JointPose pose)
    {
        pose.joint_0 = BitConverter.ToDouble(buffer, 0);
        pose.joint_1 = BitConverter.ToDouble(buffer, 8);
        pose.joint_2 = BitConverter.ToDouble(buffer, 16);
        pose.joint_3 = BitConverter.ToDouble(buffer, 24);
        pose.joint_4 = BitConverter.ToDouble(buffer, 32);
        pose.joint_5 = BitConverter.ToDouble(buffer, 40);
        pose.joint_6 = BitConverter.ToDouble(buffer, 48);
        return pose;
    }

    public static JointPose ConvertDoubleToJoints(byte[] buffer)
    {
        return new JointPose(
            BitConverter.ToDouble(buffer, 0), 
            BitConverter.ToDouble(buffer, 8), 
            BitConverter.ToDouble(buffer, 16), 
            BitConverter.ToDouble(buffer, 24), 
            BitConverter.ToDouble(buffer, 32), 
            BitConverter.ToDouble(buffer, 40), 
            BitConverter.ToDouble(buffer, 48)

        );
        return JointPose.initialPose;
    }

    public static Vector3 ConvertFloatToPosition(byte[] buffer)
    {
        return new Vector3(
            (float)BitConverter.ToSingle(buffer, 1), 
            (float)BitConverter.ToSingle(buffer, 5), 
            (float)BitConverter.ToSingle(buffer, 9)
            );
    }
    
    
    
    //todo: use paxckage byte
    public static Vector3 ConvertDoubleToPosition(byte[] buffer)
    {
        return new Vector3(
            -(float)BitConverter.ToDouble(buffer, 0), 
            (float)BitConverter.ToDouble(buffer, 8), 
            (float)BitConverter.ToDouble(buffer, 16)
        );
    }
    
    public static Vector3 Test(byte[] buffer)
    {
        return new Vector3(
            (float)BitConverter.ToDouble(buffer, 0), 
            (float)BitConverter.ToDouble(buffer, 8), 
            (float)BitConverter.ToDouble(buffer, 16)
        );
    }
    

    
    public static byte[] CreateBytePackageFromVector(Vector3 vector)
    {
        byte[] outgoingArray = new byte[13]; //3 floats, each has a float value with 4 bytes (28 bytes total) and one type byte
        byte[] x = BitConverter.GetBytes(vector.x);
        byte[] y = BitConverter.GetBytes(vector.y);
        byte[] z = BitConverter.GetBytes(vector.z);

        outgoingArray[0] = 1;
        //copy bytes in outgoing bytes
        for (int i = 0; i < 4; i++)
        {
            outgoingArray[1 + i] = x[i];
            outgoingArray[5 + i] = y[i];
            outgoingArray[9 + i] = z[i];
        }

        return outgoingArray;
    }

    public static byte[] CreateInitPackage()
    {
         byte[] outgoingArray = new byte[1]; //3 floats, each has a float value with 4 bytes (28 bytes total) and one type byte
         outgoingArray[0] = 23;
         return outgoingArray;
    }

    public static byte[] CreatePosePackage(JointPose jointPosePose)
    {
        byte[] outgoingArray = new byte[56]; //7 joints, each has a float value with 4 bytes (28 bytes total), one typeByte and one action type byte
        byte[] j_0 = BitConverter.GetBytes(jointPosePose.joint_0);
        byte[] j_1 = BitConverter.GetBytes(jointPosePose.joint_1);
        byte[] j_2 = BitConverter.GetBytes(jointPosePose.joint_2);
        byte[] j_3 = BitConverter.GetBytes(jointPosePose.joint_3);
        byte[] j_4 = BitConverter.GetBytes(jointPosePose.joint_4);
        byte[] j_5 = BitConverter.GetBytes(jointPosePose.joint_5);
        byte[] j_6 = BitConverter.GetBytes(jointPosePose.joint_6);

        //copy bytes in outgoing bytes
        
        for (int i = 0; i < 8; i++)
        {
            outgoingArray[i] = j_0[i];
            outgoingArray[8 + i] = j_1[i];
            outgoingArray[16 + i] = j_2[i];
            outgoingArray[24 + i] = j_3[i];
            outgoingArray[32 + i] = j_4[i];
            outgoingArray[40 + i] = j_5[i];
            outgoingArray[48  + i] = j_6[i];

           
        }
       
        //Debug.Log("Joints send: " + jointPose.joint_0 + ", " + jointPose.joint_1 + ", " + jointPose.joint_2 + ", " + jointPose.joint_3 + ", "+ jointPose.joint_4 + ", " + jointPose.joint_5 + ", "+ jointPose.joint_6 );
        
        return outgoingArray;
    }

    public static byte[] CreateConnectionRequestPackage(String ip, int port)
    {
        byte[] outgoingArray = new byte[9]; //1 package type byte, 4 bytes ip, 4 bytes int
        outgoingArray[0] = 10;
        
        System.Net.IPAddress ip_address = System.Net.IPAddress.Parse(ip);
        byte[] addressBytes = ip_address.GetAddressBytes();
        byte[] portBytes = BitConverter.GetBytes(port);
        
        for (int i = 0; i < 4; i++)
        {
            outgoingArray[i + 1] = addressBytes[i];
            outgoingArray[i + 5] = portBytes[i];
        }

        return outgoingArray;

    }

    public static byte[] CreateSettingsPackage(double speed)
    {
        byte[] outgoingArray = new byte[10]; 
        outgoingArray[0] = 14;
        byte[] speed_bytes = BitConverter.GetBytes(speed);
        for (int i = 0; i < 8; i++)
        {
            outgoingArray[PACKAGE_TYPE_SIZE + i] = speed_bytes[i];
        }

        return outgoingArray;
    }

    public static byte[] CreateJointRealtimePackage()
    {
        byte[] outgoingArray = new byte[1]; 
        outgoingArray[0] = 4;
        return outgoingArray; 
    }
    
    public static byte[] CreateImpendanceControl(double translational_stiffness, double rotational_stiffness, double duration)
    {
        byte[] outgoingArray = new byte[1]; 
        outgoingArray[0] = 14;
        return outgoingArray;
    }

    /// <summary>
    /// creates a byte package with a given joint pose. 
    /// </summary>
    /// <param name="jointPosePose">joint pose contains joints from j_0 to j_6</param>
    /// <returns>byte package with 57 bytes</returns>
    public static byte[] CreateJointPackage(JointPose jointPosePose, double speed)
    {
        byte[] outgoingArray = new byte[65]; //7 joints, each has a float value with 4 bytes (28 bytes total), one typeByte and one action type byte
        outgoingArray[0] = 2; //4 is package type for actions. See the Documentation for package types
        //todo performance: dont allocate new byte array each time
        byte[] j_0 = BitConverter.GetBytes(jointPosePose.joint_0);
        byte[] j_1 = BitConverter.GetBytes(jointPosePose.joint_1);
        byte[] j_2 = BitConverter.GetBytes(jointPosePose.joint_2);
        byte[] j_3 = BitConverter.GetBytes(jointPosePose.joint_3);
        byte[] j_4 = BitConverter.GetBytes(jointPosePose.joint_4);
        byte[] j_5 = BitConverter.GetBytes(jointPosePose.joint_5);
        byte[] j_6 = BitConverter.GetBytes(jointPosePose.joint_6);
        byte[] _speed = BitConverter.GetBytes(speed);
        
        //copy bytes in outgoing bytes
        
        for (int i = 0; i < 8; i++)
        {
            outgoingArray[PACKAGE_TYPE_SIZE + i] = j_0[i];
            outgoingArray[PACKAGE_TYPE_SIZE + 8 + i] = j_1[i];
            outgoingArray[PACKAGE_TYPE_SIZE + 16 + i] = j_2[i];
            outgoingArray[PACKAGE_TYPE_SIZE + 24 + i] = j_3[i];
            outgoingArray[PACKAGE_TYPE_SIZE + 32 + i] = j_4[i];
            outgoingArray[PACKAGE_TYPE_SIZE + 40 + i] = j_5[i];
            outgoingArray[PACKAGE_TYPE_SIZE + 48  + i] = j_6[i];
            outgoingArray[PACKAGE_TYPE_SIZE + 56  + i] = _speed[i];
           
        }
       
        Debug.Log("Joints send: " + jointPosePose.joint_0 + ", " + jointPosePose.joint_1 + ", " + jointPosePose.joint_2 + ", " + jointPosePose.joint_3 + ", "+ jointPosePose.joint_4 + ", " + jointPosePose.joint_5 + ", "+ jointPosePose.joint_6 );
        
        return outgoingArray;
    }
    
    /// <summary>
    /// creates a byte package with a given cartesian pose
    /// </summary>
    /// <param name="cartesianPose">end effectors cartesian position and cartesian orientation</param>
    /// <returns>byte package with 65 bytes</returns>
    public static byte[] CreatePosePackage(CartesianPose cartesianPose, double speed)
    {
        
        var outgoingArray = new byte[73];
        outgoingArray[0] = 1; //4 is package type for actions. See the Documentation for package types

        var x = BitConverter.GetBytes((double)cartesianPose.position.z);
        var y = BitConverter.GetBytes(-(double)cartesianPose.position.x);
        var z = BitConverter.GetBytes((double)cartesianPose.position.y);
        
        var rotation_x = BitConverter.GetBytes((double)cartesianPose.orientation.x);
        var rotation_y = BitConverter.GetBytes((double)cartesianPose.orientation.y);
        var rotation_z = BitConverter.GetBytes((double)cartesianPose.orientation.z);
        var _speed = BitConverter.GetBytes((double)speed);

        for (var i = 0; i < 8; i++)
        {
            outgoingArray[PACKAGE_TYPE_SIZE + i] = x[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 8] = y[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 16] = z[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 24] = rotation_x[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 32] = rotation_y[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 40] = rotation_z[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 48] = _speed[i];
        }

        return outgoingArray;
    }

    public static byte[] CreateOrientation(double roll, double pitch, double yaw, double time)
    {
        var outgoingArray = new byte[1 + 8 * 4];
        outgoingArray[0] = 18; //4 is package type for actions. See the Documentation for package types

        var _roll = BitConverter.GetBytes((double)roll);
        var _pitch = BitConverter.GetBytes((double)pitch);
        var _yaw = BitConverter.GetBytes((double)yaw);
        var _time = BitConverter.GetBytes((double)time);

        for (var i = 0; i < 8; i++)
        {
            outgoingArray[PACKAGE_TYPE_SIZE + i]      = _roll[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 8]  = _pitch[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 16] = _yaw[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 24] = _time[i];
        }

        return outgoingArray;
    }

    /// <summary>
    /// creates a byte package with parameters required for gripping motion
    /// </summary>
    /// <param name="objectWidth">size of the object to grasp [m]</param>
    /// <param name="gripperSpeed">desired closing speed [m/s]</param>
    /// <param name="gripperForce">desired grasping force [N]</param>
    /// <param name="innerEpsilon">maximum tolerated deviation when the actual grasped width is smaller than the commanded grasp width. [m]</param>
    /// <param name="outerEpsilon">maximum tolerated deviation when the actual grasped width is larger than the commanded grasp width. [m]</param>
    /// <returns>byte package with 42 bytes</returns>
    public static byte[] CreateGraspPackage(double objectWidth, double gripperSpeed, double gripperForce, double innerEpsilon,double outerEpsilon)
    {
        var outgoingArray = new byte[42];
        outgoingArray[0] = 7; //4 is package type for actions. See the Documentation for package types
        
        
        var object_width_pck = BitConverter.GetBytes(objectWidth);
        var gripper_speed_pck = BitConverter.GetBytes(gripperSpeed);
        var gripper_force_pck = BitConverter.GetBytes(gripperForce);
        var inner_epsilon_pck = BitConverter.GetBytes(innerEpsilon);
        var outer_epsilon_pck = BitConverter.GetBytes(outerEpsilon);
        
        for (int i = 0; i < 8; i++)
        {
            outgoingArray[PACKAGE_TYPE_SIZE + i] = object_width_pck[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 8] = gripper_speed_pck[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 16] = gripper_force_pck[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 24] = inner_epsilon_pck[i];
            outgoingArray[PACKAGE_TYPE_SIZE + i + 32] = outer_epsilon_pck[i];
        }
        
        return outgoingArray;
    }

    /// <summary>
    /// creates a package for releasing grasped object
    /// </summary>
    /// <returns>byte package with 1 byte</returns>
    public static byte[] CreateReleasePackage()
    {
        byte[] outgoingArray = new byte[1];
        outgoingArray[0] = 8; //4 is package type for actions. See the Documentation for package types
        return outgoingArray;
    }

    public static byte[] CreateStatusPackage(StatusCode statusCode)
    {
        byte[] outgoingArray = new byte[2];
        outgoingArray[0] = 5;
        switch(statusCode)
        {
            case StatusCode.READY:
                outgoingArray[1] = 1;
                Debug.Log("Ready");
                break;
            case StatusCode.NOT_CONNECTED: 
                outgoingArray[1] = 2;
                Debug.Log("Not Connected"); 
                break;
            default:
                outgoingArray[1] = 0;
                Debug.Log("Empty");
                break;
        }



        outgoingArray[1] = 0;

        return outgoingArray;
        throw  new NotImplementedException();
    }

    //todo rewrite/new
    public static byte[] CreatePackage(PackageType packageType)
    {
        int payload_size = 0; //bytes
        byte signal_bytes = 0;
        byte[] payload = new byte[0];
        if (packageType == PackageType.PING)
        {
            payload_size = 0;
            signal_bytes = 0;
        }
        else if(packageType == PackageType.STATUS)
        {
            payload_size = 1;
            payload = new byte[1];
            signal_bytes = 5;
            
        }

        byte[] outgoingArray = new byte[PACKAGE_TYPE_SIZE + payload_size];
        outgoingArray[0] = signal_bytes;

        for (int i = 0; i < payload_size; i++)
        {
            outgoingArray[i+1] = payload[i];
        }
        
        return outgoingArray;
    }
}

public enum PackageType
{
    PING,
    POSITION_UPDATE,
    ORIENTATION_UPDATE,
    CARTESIAN_POSE_UPDATE,
    ACTION,
    STATUS,
    JOINT_UPDATE,
    EMPTY
    
}

public enum ActionCode
{
    CONNECT,
    GET_STATUS,
    PTP,
    CONTINIOUS_PATH,
    GRAB,
    RELEASE,
    INIT,
    GRIPPER_WIDTH,
}

public enum StatusCode
{
    READY,
    NOT_CONNECTED,
    INVALID_MODE,
    TIMEOUT,
    
}

//TODO: Package
public struct Package
{
    private Header header;
    private Payload payload;
}

public struct Header
{
    private PackageType type;
}

public struct Payload
{
    private byte[] payload;

}
