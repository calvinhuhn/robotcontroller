﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class FrankaNetwork : MonoBehaviour
{
    public Vector3 position_endeffector;
    private bool streaming = true;
    private Thread receiverThread;
    #region Network
    public String ip;
    public int receivingPort = 514;
    public int sendingPort = 513;
    private Socket receivingNetworkSocket;
    private Socket sendingNetworkSocket;
    private IPAddress broadcast;
    private IPEndPoint ep;
    private byte[] buffer = new byte[64];
    #endregion
    
    void Start()
    {
        InitNetworkSockets();
        receiverThread = new Thread(StartUpdateLoop);
        receiverThread.Start();
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            Debug.Log("Test sending to:" + CartesianPose.initialPose.position);
            MoveRobotToPosition(CartesianPose.initialPose.position);
        }
    }
    
    private void OnDestroy()
    {
        //todo: don't abort, join instead
        receiverThread.Abort();
        receivingNetworkSocket.Close();
        sendingNetworkSocket.Close();
    }

    public void MoveRobotToPosition(Vector3 position)
    {
        byte[] sendbuf = Converter.CreateBytePackageFromVector(position);
        sendingNetworkSocket.SendTo(sendbuf, ep);
    }

    /// <summary>
    /// starts update loop
    /// </summary>
    /// <exception cref="NotImplementedException"></exception>
    public void StartUpdateLoop()
    {
        do
        {
            receivingNetworkSocket.Receive(buffer);
            switch (buffer[0])
            {
                case 1:
                    //position update
                    position_endeffector = Converter.ConvertFloatToPosition(buffer);
                    break;
                case 2:
                    //orientation update
                    throw new NotImplementedException();
                    break;
                case 99:
                    //end streaming
                    streaming = false;
                    break;
            }

        } while (streaming);
    }

    /// <summary>
    /// to get the current position, use UpdateEndeffectorPosition()
    /// </summary>
    /// <returns>last known end effector position</returns>
    public Vector3 GetLastEndeffectorPosion()
    {
        return position_endeffector;
    }

    /// <summary>
    /// initializing network sockets
    /// </summary>
    private void InitNetworkSockets()
    {
        Debug.Log("initializing network sockets");
        try
        {
            //init receiver
            receivingNetworkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
                ProtocolType.Udp);
            var remoteEP = new IPEndPoint(IPAddress.Any, receivingPort);
            receivingNetworkSocket.Bind(remoteEP); 
        
            //init sender
            sendingNetworkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
                ProtocolType.Udp);

            IPAddress serverAddr = IPAddress.Parse(ip);
            ep = new IPEndPoint(serverAddr, sendingPort);
            
        }
        catch (Exception e)
        {
            
        }

    }
}
