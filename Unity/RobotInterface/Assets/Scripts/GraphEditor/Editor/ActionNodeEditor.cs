﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(ActionNode))]
public class ActionNodeEditor : NodeEditor {

    public override void OnHeaderGUI() {
        GUI.color = Color.white;
        ActionNode node = target as ActionNode;
        MotionGraph graph = node.graph as MotionGraph;
        if (graph.current == node) GUI.color = Color.blue;
        string title = target.name;
        GUILayout.Label(title, NodeEditorResources.styles.nodeHeader, GUILayout.Height(30));
        GUI.color = Color.white;
    }

    public override void OnBodyGUI() {
        base.OnBodyGUI();
        ActionNode node = target as ActionNode;
        MotionGraph graph = node.graph as MotionGraph;
        if (GUILayout.Button("MoveNext Node")) node.MoveNext();
        if (GUILayout.Button("Continue Graph")) graph.Continue();
        if (GUILayout.Button("Set as current"))
        {
            graph.current = node;
            node.Execute();
        }
        if (GUILayout.Button("Preview"))
        {
            graph.current.Preview();
        }

        
    }
}
