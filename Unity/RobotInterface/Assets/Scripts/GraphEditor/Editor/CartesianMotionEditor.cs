﻿using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(CartesianMotion))]
public class CartesianMotionEditor : NodeEditor {
    

    public override void OnBodyGUI() {
        base.OnBodyGUI();
        ActionNode node = target as ActionNode;
        MotionGraph graph = node.graph as MotionGraph;
       
        if (GUILayout.Button("MoveNext Node")) node.MoveNext();
        if (GUILayout.Button("Continue Graph")) graph.Continue();
        if (GUILayout.Button("Set as current"))
        {
            graph.current = node;
            node.Execute();
        }
       
        
    }
   
}