﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class CartesianPoseNode : Node
{
    [Input] public float position_x, position_y, position_z;
    [Input] public float orientation_x, orientation_y, orientation_z;
    [Output] public CartesianPose cartesian_pose;
    

    public override object GetValue(XNode.NodePort port) {
        cartesian_pose.position.x = GetInputValue<float>("position_x", this.position_x);
        cartesian_pose.position.y = GetInputValue<float>("position_y", this.position_y);
        cartesian_pose.position.z = GetInputValue<float>("position_z", this.position_z);
        cartesian_pose.orientation.x = GetInputValue<float>("orientation_x", this.orientation_x);
        cartesian_pose.orientation.y = GetInputValue<float>("orientation_y", this.orientation_y);
        cartesian_pose.orientation.z = GetInputValue<float>("orientation_z", this.orientation_z);
       
        return cartesian_pose;
    }
    
}
