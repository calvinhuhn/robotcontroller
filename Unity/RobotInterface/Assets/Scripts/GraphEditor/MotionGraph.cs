﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNode;


/// <summary> Defines an example nodegraph that can be created as an asset in the Project window. </summary>
[Serializable, CreateAssetMenu(fileName = "New Motion Graph", menuName = "Movement/Motion Graph")]
public class MotionGraph : XNode.NodeGraph
{
    // The current "active" node
    public ActionNode current;

    public void Continue() {
        current.MoveNext();
    }
    
}