﻿
using System;
using UnityEngine;
using XNode;

public abstract class ActionNode : Node
{
    public abstract void MoveNext();
    public abstract void OnEnter();
    public abstract void Execute();

    public abstract void Preview();
    
    [Serializable]
    public class Empty
    {
    }
}
