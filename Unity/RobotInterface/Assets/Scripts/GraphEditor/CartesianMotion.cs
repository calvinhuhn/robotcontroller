﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class CartesianMotion : ActionNode
{
    [Input] public Empty enter;
    [Output] public Empty exit;
    [Input] public float speed;

    public override void MoveNext() {
        MotionGraph fmGraph = graph as MotionGraph;

        if (fmGraph.current != this) {
            Debug.LogWarning("Node isn't active");
            return;
        }

        NodePort exitPort = GetOutputPort("exit");

        if (!exitPort.IsConnected) {
            Debug.LogWarning("Node isn't connected");
            return;
        }

        ActionNode node = exitPort.Connection.node as ActionNode;
        node.OnEnter();
    }

    public override void OnEnter() {
        MotionGraph fmGraph = graph as MotionGraph;
        fmGraph.current = this;
        Execute();
        
    }

    public override void Execute()
    {
        cartesianPose = GetInputValue<CartesianPose>("cartesian_pose", this.cartesianPose);
        speed = GetInputValue<float>("speed", this.speed);
        FrankaPanda.GetInstance().MoveToPoint(cartesianPose, speed);
    }

    public override void Preview()
    {
        cartesianPose = GetInputValue<CartesianPose>("cartesian_pose", this.cartesianPose);
        //TODO
        FrankaPanda.GetInstance()._cartesianPose = cartesianPose;
    }


    [Input] public CartesianPose cartesianPose;
    public override object GetValue(XNode.NodePort port) {
        cartesianPose = GetInputValue<CartesianPose>("cartesian_pose", this.cartesianPose);
        return cartesianPose;
    }
    
   

    
       
    [ContextMenu("Initial Pose")]
    public void Init()
    {
        cartesianPose = CartesianPose.initialPose;
    }
    
    [ContextMenu("Pickup Pose")]
    public void Pickup()
    {
        cartesianPose = CartesianPose.PickupPose;
    }
}

