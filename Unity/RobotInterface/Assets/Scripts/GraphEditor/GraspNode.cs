﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class GraspNode : ActionNode
{
	[Input] public Empty enter;
	[Output] public Empty exit;
	
	 public float objectWidth, force, speed, epsilon_inner, epsilon_outer;
	
	public override void MoveNext()
	{
		MotionGraph fmGraph = graph as MotionGraph;

		if (fmGraph.current != this) {
			Debug.LogWarning("Node isn't active");
			return;
		}

		NodePort exitPort = GetOutputPort("exit");

		if (!exitPort.IsConnected) {
			Debug.LogWarning("Node isn't connected");
			return;
		}

		ActionNode node = exitPort.Connection.node as ActionNode;
		node.OnEnter();
	}

	public override void OnEnter() {
		MotionGraph fmGraph = graph as MotionGraph;
		fmGraph.current = this;
		Execute();
        
	}

	public override void Execute()
	{
		objectWidth = GetInputValue<float>("objectWidth", this.objectWidth);
		speed = GetInputValue<float>("speed", this.speed);
		force = GetInputValue<float>("force", this.force);
		epsilon_inner = GetInputValue<float>("epsilon_inner", this.epsilon_inner);
		epsilon_outer = GetInputValue<float>("epsilon_outer", this.epsilon_outer);
		FrankaPanda.GetInstance().Grasp(objectWidth, speed, force, epsilon_inner, epsilon_outer);
	}
	
	public override void Preview()
	{
		
		
	}
	

	// Return the correct value of an output port when requested
	public override object GetValue(NodePort port) {
		return null; // Replace this
	}
}