﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using XNode;

public class JointMotion : ActionNode {
	[Input] public Empty enter;
	[Output] public Empty exit;
	[FormerlySerializedAs("jointPose")] [Input] public JointPose jointPosePose;
	[Input] public float speed;
	
	public override void MoveNext()
	{
		MotionGraph fmGraph = graph as MotionGraph;

		if (fmGraph.current != this) {
			Debug.LogWarning("Node isn't active");
			return;
		}

		NodePort exitPort = GetOutputPort("exit");

		if (!exitPort.IsConnected) {
			Debug.LogWarning("Node isn't connected");
			return;
		}

		ActionNode node = exitPort.Connection.node as ActionNode;
		node.OnEnter();
	}

	public override void OnEnter() {
		MotionGraph fmGraph = graph as MotionGraph;
		fmGraph.current = this;
		Execute();
        
	}

	public override void Execute()
	{
		jointPosePose = GetInputValue<JointPose>("jointPose", this.jointPosePose);
		speed = GetInputValue<float>("speed", this.speed);
		FrankaPanda.GetInstance().MoveJoints(jointPosePose, (double)speed);
	}
	
//TODO: Not working. Joints are not showing
	public override void Preview()
	{
		jointPosePose = GetInputValue<JointPose>("jointPose", this.jointPosePose);
		FrankaPanda.GetInstance().jointPose = this.jointPosePose;
		FrankaPanda.GetInstance().UpdateJoints();
		
	}
	

	
	[ContextMenu("Initial Pose")]
	public void Init()
	{
		jointPosePose = JointPose.initialPose;
	}
	
}