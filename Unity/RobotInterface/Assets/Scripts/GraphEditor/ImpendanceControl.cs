﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class ImpendanceControl : ActionNode
{
    [Input] public Empty enter;
    [Output] public Empty exit;
    [Input] public float translational_stiffness, rotational_stiffness, stiffness, damping;

    public override void MoveNext()
    {
        MotionGraph fmGraph = graph as MotionGraph;

        if (fmGraph.current != this)
        {
            Debug.LogWarning("Node isn't active");
            return;
        }

        NodePort exitPort = GetOutputPort("exit");

        if (!exitPort.IsConnected)
        {
            Debug.LogWarning("Node isn't connected");
            return;
        }

        ActionNode node = exitPort.Connection.node as ActionNode;
        node.OnEnter();
    }

    public override void OnEnter()
    {
        MotionGraph fmGraph = graph as MotionGraph;
        fmGraph.current = this;
        Execute();
    }


    public override void Execute()
    {
        //cartesianPose = GetInputValue<CartesianPose>("cartesian_pose", this.cartesianPose);
        //speed = GetInputValue<float>("speed", this.speed);
        FrankaPanda.GetInstance().StartCartesianImpendanceControl();
    }

    public override void Preview()
    {
    }

    [ContextMenu("No Gravity")]
    public void Init()
    {
        stiffness = 0;
    }
}