﻿using UnityEngine;
using XNode;

    public class PauseNode : ActionNode
    {
        [Input] public Empty enter;
        [Output] public Empty exit;
        public override void MoveNext()
        {
            MotionGraph fmGraph = graph as MotionGraph;

            if (fmGraph.current != this) {
                Debug.LogWarning("Node isn't active");
                return;
            }

            NodePort exitPort = GetOutputPort("exit");

            if (!exitPort.IsConnected) {
                Debug.LogWarning("Node isn't connected");
                return;
            }

            ActionNode node = exitPort.Connection.node as ActionNode;
            node.OnEnter();
        }

        public override void OnEnter()
        {
            MotionGraph fmGraph = graph as MotionGraph;
            fmGraph.current = this;
            Execute();
        }

        public override void Execute()
        {
            //FrankaPanda.GetInstance().LocalRotation(roll, pitch, yaw, duration);
        }

        public override void Preview()
        {
            
        }
    }
