﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Action
{
    private FrankaPanda.OperatingMode _operatingMode;
    
    private CartesianPose _cartesianPose;
    private JointPoses _jointPoses;

    public CartesianPose CartesianPose
    {
        get => _cartesianPose;
        set => _cartesianPose = value;
    }

    public JointPoses JointPoses
    {
        get => _jointPoses;
        set => _jointPoses = value;
    }
    
    public Action(CartesianPose cartesianPose)
    {
        _operatingMode = FrankaPanda.OperatingMode.CartesianSpace;
        _cartesianPose = cartesianPose;
        _jointPoses = JointPoses.INITIAL;
    }
    
    public Action(JointPoses jointPoses)
    {
        _operatingMode = FrankaPanda.OperatingMode.JointSpace;
        _cartesianPose = CartesianPose.initialPose;
        _jointPoses = jointPoses;
    }
    
   

    public Action(FrankaPanda.OperatingMode operatingMode, CartesianPose cartesianPose, JointPoses jointPoses)
    {
        _operatingMode = operatingMode;
        _cartesianPose = cartesianPose;
        _jointPoses = jointPoses;
    }
}
