﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FrankaPanda))]
public class JointUpdater : MonoBehaviour
{
    public FrankaPanda panda;
    // Start is called before the first frame update
    void Start()
    {
        panda = this.GetComponent<FrankaPanda>();
        
    }
    private const float rad_to_deg = 180 / Mathf.PI;

    public GameObject joint_0;
    public GameObject joint_1;
    public GameObject joint_2;
    public GameObject joint_3;
    public GameObject joint_4;
    public GameObject joint_5;
    public GameObject joint_6;

    

    // Update is called once per frame
    void Update()
    {
        //ApplyJoints();
    }
    public void ApplyJoints() {
      

        joint_0.transform.transform.localRotation = Quaternion.Euler(-90, (float)-panda.jointPose.joint_0 * rad_to_deg, 0);
        joint_1.transform.localRotation = Quaternion.Euler(-90, (float)-panda.jointPose.joint_1 * rad_to_deg, 0);
        joint_2.transform.localRotation = Quaternion.Euler(180, (float)panda.jointPose.joint_2 * rad_to_deg, 0);
        joint_3.transform.localRotation = Quaternion.Euler(90 + (float)panda.jointPose.joint_3 * rad_to_deg, 90, 90);
        joint_4.transform.localRotation = Quaternion.Euler(-90 + (float)panda.jointPose.joint_4 * rad_to_deg, -90, 90);
        joint_5.transform.localRotation = Quaternion.Euler(90 + (float)panda.jointPose.joint_5 * rad_to_deg, 90, 90);
        joint_6.transform.localRotation = Quaternion.Euler(-45 + (float)(-panda.jointPose.joint_6) * rad_to_deg, -90, -90); 


        
    }

    public void ApplyJoints(JointPose pose)
    {
        joint_0.transform.transform.localRotation = Quaternion.Euler(-90, (float)-pose.joint_0 * rad_to_deg, 0);
        joint_1.transform.localRotation = Quaternion.Euler(-90, (float)-pose.joint_1 * rad_to_deg, 0);
        joint_2.transform.localRotation = Quaternion.Euler(180, (float)pose.joint_2 * rad_to_deg, 0);
        joint_3.transform.localRotation = Quaternion.Euler(90 + (float)pose.joint_3 * rad_to_deg, 90, 90);
        joint_4.transform.localRotation = Quaternion.Euler(-90 + (float)pose.joint_4 * rad_to_deg, -90, 90);
        joint_5.transform.localRotation = Quaternion.Euler(90 + (float)pose.joint_5 * rad_to_deg, 90, 90);
        joint_6.transform.localRotation = Quaternion.Euler(-45 + (float)(-pose.joint_6) * rad_to_deg, -90, -90); 

    }


}
