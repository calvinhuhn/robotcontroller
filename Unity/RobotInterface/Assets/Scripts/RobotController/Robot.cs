﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Robot : MonoBehaviour
{
    [SerializeField]
    public Settings settings;
    public abstract void MoveToPoint();

    public abstract void MoveJoints();
    public abstract void Connect();

    public abstract void LoadSettings();

    public abstract void Ping();
    
    public abstract void SendStatus();

    public abstract void SendSettings();

    public abstract void Move();

}
