﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gripper : MonoBehaviour
{

    
    [Range(0, 0.08f)]
    public double gripperWidth = 0.0f;
    [HideInInspector]
    public double updateWidth = -1.0f;



    [Range(0, 1)]
    public double force = 0.0f;

    public bool grasp = false;
    public bool release = false;



    // Start is called before the first frame update
    void Start()
    {
        //Cartesian Space Operation
        //if (operatingMode == OperatingMode.CartesianSpace)
        {
            //gameObject.AddComponent<TransmitPosition>();
        }
        //else {  //Joint Space Operation
            //gameObject.AddComponent<JointPositionTransmission>();
       // }
    }

    // Update is called once per frame
    void Update()
    {
        
        //this.position = this.transform.position;
        if (grasp == true)
        {
            grasp = false;
            updateWidth = gripperWidth;
        }

        if (release == true)
        {
            release = false;
            updateWidth = -2.0f;
        }

    }

}
