﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

//[ExecuteInEditMode]
[RequireComponent(typeof(Gripper))]
public class FrankaPanda : Robot
{
    public string ConfigFileName = "";
    
    public bool debugEnabled;
    public bool loadFromFile;
    public bool saveSettings;
    public bool updateJoints = true;
    
    private StatusCode statusCode;
    private Socket receivingNetworkSocket;
    private Socket sendingNetworkSocket;
    IPEndPoint endPoint;
    private Gripper gripper;
    public JointUpdater jointUpdate;
    static public int rec_port = 55505; 
    private UdpClient udpServer;

    IPEndPoint remoteEP;
    //TODO
    public bool moveToInitPositionOnStartup = true;
    
    [HideInInspector]
    public Vector3 endeffector;
    
    [System.Serializable]
    public enum OperatingMode {
        CartesianSpace,
        JointSpace,
        JointRealTime
    };

    public void Awake()
    {
        Debug.Log("Startup");
        jointUpdate = FindObjectOfType<JointUpdater>();

        FrankaPanda.instance = this;
    }

    public bool restrictCartesianArea = true;
    public Vector3 restrictionPoint;
    public Vector3 scale;
    private GameObject cube;

    public OperatingMode operatingMode;
    public int realtimeRefreshRate = 100;
    
    public CartesianPose _cartesianPose;

    [FormerlySerializedAs("jointPosePose")] public JointPose jointPose;
    public Vector3 pose = new Vector3(0,0,0);
    
    [Range(0.01f, 10.0f)]
    public double speed = 1.0;
    private Thread updateThread;
    private Thread networkThread;

    [Range(0, 0.08f)]
    public double objectWidth = 0.3f;
    public double gripperSpeed = 0.5;
    public double force = 0.4f;
    public double epsilon_inner = 0.005;
    public double epsolon_outer = 0.005;
    
    public override void MoveToPoint()
    {
        MoveToPoint(_cartesianPose, speed);
    }

    private bool realtime = false;
    public void StartRealtimeJointMotion()
    {
        byte[] buffer = Converter.CreateJointRealtimePackage();
        receivingNetworkSocket.SendTo(buffer, endPoint);
        
        networkThread = new Thread(sendJointUpdates);
        Debug.Log("realtime environment set to: true");
        realtime = true;
        Debug.Log("starting new network thread...");
        networkThread.Start();
    }

    public Vector3 GetPosition()
    {
        //todo vbad
        if (statusCode == StatusCode.READY)
        {
            byte[] buffer = new byte[64];
            buffer[0] = 19;
            sendingNetworkSocket.SendTo(buffer, endPoint);

            receivingNetworkSocket.Receive(buffer);
            pose = Converter.ConvertDoubleToPosition(buffer);

        }

        _cartesianPose.position = pose;
        return pose;
        
    }

    public void SetRestrictionArea()
    {
        restrictCartesianArea = true;
        
        cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = restrictionPoint;
        cube.transform.localScale = scale;
        
        Destroy(cube.GetComponent<MeshRenderer>());
        Destroy(cube.GetComponent<MeshCollider>());
        
    }

    public void RemoveRestrictionArea()
    {
        restrictCartesianArea = false;
        GameObject.Destroy(cube);
    }

    private JointPose GetCurrentJointPose()
    {
        if (statusCode == StatusCode.READY)
        {
            byte[] buffer = new byte[64];
            buffer[0] = 22;
            sendingNetworkSocket.SendTo(buffer, endPoint);

            receivingNetworkSocket.Receive(buffer);
            jointPose = Converter.UpdateJoints(buffer, ref jointPose);
        }
        return jointPose;
    }



    public void StopRealtimeJointMotion()
    {
        Debug.Log("stopping realtime mode...");
        realtime = false;
        Debug.Log("realtime environment set to: false");
        networkThread.Join();
        Debug.Log("joining network thread...");

        byte[] buffer = BitConverter.GetBytes(-2000.0);
        receivingNetworkSocket.SendTo(buffer, endPoint);
    }

    public void sendJointUpdates()
    {
        while (realtime)
        {
            if (!realtime)
            {
                break;
            }

            byte[] buffer = Converter.CreatePosePackage(jointPose);
            receivingNetworkSocket.SendTo(buffer, endPoint);
            Thread.Sleep(realtimeRefreshRate);
        }
       
    }

    /// <summary>
    /// rotates the end effector  
    /// </summary>
    /// <param name="roll">roll in radians</param>
    /// <param name="pitch">pitch in radians</param>
    /// <param name="yaw">yaw in radians</param>
    /// <param name="duration">in seconds for rotation to be completed</param>
    public void LocalRotation(double roll, double pitch, double yaw, double duration)
    {
        byte[] buffer = Converter.CreateOrientation(roll, pitch, yaw, duration);
        receivingNetworkSocket.SendTo(buffer, endPoint);
    }

    /// <summary>
    /// moves the end effector to a given point in cartesian space
    /// </summary>
    /// <param name="cartesianPose">point in cartesian pose</param>
    /// <param name="speed">in seconds for rotation to be completed</param>
    /// <exception cref="Exception">throws error if robot is not connected</exception>
    public void MoveToPoint(CartesianPose cartesianPose, double speed)
    {
        if (statusCode != StatusCode.READY)
        {
            throw new Exception("Robot not connected: please check the internet connection or the robot state");
        }

        if (restrictCartesianArea)
        {
            if (!PositionValid(cartesianPose))
            {
                Debug.LogWarning("Position not within defined boundaries");
                return;
            }
        }

        byte[] buffer = Converter.CreatePosePackage(cartesianPose, speed);
        receivingNetworkSocket.SendTo(buffer, endPoint);
    }

    /// <summary>
    /// moves robot to initial pose
    /// </summary>
    public void MoveToInitPosition()
    {
        byte[] buffer = Converter.CreateInitPackage();
        receivingNetworkSocket.SendTo(buffer, endPoint);
    }

    private const double error = 0.00001;
    
    /// <summary>
    /// checks if cartesian pose is within the given bounds.
    /// </summary>
    /// <param name="cartesianPose">the point to be checked</param>
    /// <returns>true, if position is within restricted area</returns>
    public bool PositionValid(CartesianPose cartesianPose)
    {
        if (cartesianPose.position.x > restrictionPoint.x + (scale.x * 0.5) + error ||
            cartesianPose.position.x < restrictionPoint.x - (scale.x * 0.5) - error)
        {
            return false;
        }
        
        if (cartesianPose.position.y > restrictionPoint.y + (scale.y * 0.5) + error ||
            cartesianPose.position.y < restrictionPoint.y - (scale.y * 0.5) - error)
        {
            return false;
        }
        
        if (cartesianPose.position.z > restrictionPoint.z + (scale.z * 0.5) + error ||
            cartesianPose.position.z < restrictionPoint.z - (scale.z * 0.5) - error )
        {
            return false;
        }
        
        return true;
    }

    /// <summary>
    /// moves to each corner of the restricted area  
    /// </summary>
    public void TestBoundaries()
    {
        MoveJoints(JointPose.initialPose, 1.0);
        
        _cartesianPose = CartesianPose.initialPose;
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x + (scale.x * 0.5)), 
            (float)(restrictionPoint.y + (scale.y * 0.5)), 
            (float)(restrictionPoint.z + (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x - (scale.x * 0.5)), 
            (float)(restrictionPoint.y + (scale.y * 0.5)), 
            (float)(restrictionPoint.z + (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x - (scale.x * 0.5)), 
            (float)(restrictionPoint.y + (scale.y * 0.5)), 
            (float)(restrictionPoint.z - (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x + (scale.x * 0.5)), 
            (float)(restrictionPoint.y + (scale.y * 0.5)), 
            (float)(restrictionPoint.z - (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        
        MoveJoints(JointPose.initialPose, 1.0);
        
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x + (scale.x * 0.5)), 
            (float)(restrictionPoint.y - (scale.y * 0.5)), 
            (float)(restrictionPoint.z + (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x - (scale.x * 0.5)), 
            (float)(restrictionPoint.y - (scale.y * 0.5)), 
            (float)(restrictionPoint.z + (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x - (scale.x * 0.5)), 
            (float)(restrictionPoint.y - (scale.y * 0.5)), 
            (float)(restrictionPoint.z - (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        _cartesianPose.position = new Vector3(
            (float)(restrictionPoint.x + (scale.x * 0.5)), 
            (float)(restrictionPoint.y - (scale.y * 0.5)), 
            (float)(restrictionPoint.z - (scale.z * 0.5)));
        MoveToPoint(_cartesianPose, 5.0);
        
        MoveJoints(JointPose.initialPose, 1.0);
    }

    public override void MoveJoints()
    {
        Debug.Log(settings.name + " moves Joints to: ");

        //todo remove from joint
        //SendSettings();
        
        byte[] buffer = Converter.CreateJointPackage(jointPose, 0.5);
        receivingNetworkSocket.SendTo(buffer, endPoint);
    }

    public void StartCartesianImpendanceControl()
    {
        byte[] buffer = Converter.CreateImpendanceControl(0.0, 0.0, 0.0);
        receivingNetworkSocket.SendTo(buffer, endPoint);
    }

    /// <summary>
    /// moves the robot to a joint pose
    /// </summary>
    /// <param name="jointsPose">joints 0 - 6</param>
    /// <param name="speed">relative speed between 0.0 and 1.0</param>
    public void MoveJoints(JointPose jointsPose, double speed)
    {
        byte[] buffer = Converter.CreateJointPackage(jointsPose, speed);
        if (debugEnabled == true) Debug.Log("Send to: " + endPoint.ToString());
        receivingNetworkSocket.SendTo(buffer, endPoint);
    }


    public override void Connect()
    {
        if (statusCode == StatusCode.NOT_CONNECTED)
        {
            Debug.LogWarning("Not Connected!");
            StartCoroutine("EstablishConnection");
        }
        if (statusCode == StatusCode.READY)
        {
            updateThread = new Thread(GetStatusUpdate);
            if(moveToInitPositionOnStartup)
                MoveToInitPosition();
        }
        
    }


    

    private void Start()
    {
        statusCode = StatusCode.NOT_CONNECTED;
        gripper = GetComponent<Gripper>();
        if (loadFromFile)
        {
            LoadSettings();
        } 
        
        if (saveSettings)
        {
            SaveSettings();
        }

        InitUDPSocket();
        FrankaPanda.instance = this;
        

        Connect();
        jointPose = JointPose.initialPose;
        

    }

    void Update()
    {
        if (statusCode == StatusCode.READY && updateJoints)
        {
            
            GetCurrentJointPose();    
            jointUpdate.ApplyJoints(jointPose);
        }

        if (Input.GetKeyUp(KeyCode.V))
        {
            TestBoundaries();
        }
    }
    
    public string GetLocalIPv4()
    {
        return Dns.GetHostEntry(Dns.GetHostName())
            .AddressList.First(
                f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            .ToString();
    }    


    IEnumerator EstablishConnection() {
        for(;;) {
            Debug.Log("Sending connection request...");
            
            Debug.Log(GetLocalIPv4());
            String ip = GetLocalIPv4();
            byte[] buffer = Converter.CreateConnectionRequestPackage(GetLocalIPv4(), rec_port);
            sendingNetworkSocket.SendTo(buffer, endPoint);
            
            byte[] bytes = new byte[256];
            
            receivingNetworkSocket.ReceiveTimeout = 1000;
            try {
                Debug.Log("Waiting for ack");
                receivingNetworkSocket.Receive(bytes); // listen on port 11000
            
                if (bytes[0] == 10)
                {
                    Debug.Log("Ack received: Connection Ready");
                    statusCode = StatusCode.READY;

                }
            }
            catch (SocketException ex)
            {
                Debug.LogWarning("No robot found. Connecting again in 5 sec");
                
            }
            
            if (statusCode == StatusCode.READY)
            {
                break;
            }
            else
            {
                yield return new WaitForSeconds(5.0f);
            }
        }
    }
    
    void GetStatusUpdate() {
        for(;;) {
            byte[] b = new byte[100];
            Debug.Log("On Receive");
            //int k = sendingNetworkSocket.Receive(b);
            statusCode = StatusCode.READY;
            Debug.Log("Connection successful");
            break;

        }
    }
    private void OnDestroy()
    {
        Debug.Log("Stopping Robot");
        byte[] buffer = new byte[1];
        buffer[0] = 0;
        sendingNetworkSocket.SendTo(buffer, endPoint);
        
        Debug.Log("Closing Network sockets");
        receivingNetworkSocket.Close();
        sendingNetworkSocket.Close();
        //todo: how to cancel thread correctly
        //updateThread.Abort();
        
    }

    #region Settings
    
    
    /// <summary>
    /// saves franka panda settings in a txt file in JSON format
    /// </summary>
    public void SaveSettings()
    {
        if (File.Exists(ConfigFileName))
        {
            Debug.LogWarning(ConfigFileName +" already exists. Replacing File!");
            //return;
        }
        var sr = File.CreateText(ConfigFileName);
        string json = JsonUtility.ToJson(settings);
        sr.WriteLine (json);
        sr.Close();
    }

    /// <summary>
    /// loads settings file in JSON format
    /// </summary>
    public override void LoadSettings()
    {
        string json = "";
        if(File.Exists(ConfigFileName)){
            var sr = File.OpenText(ConfigFileName);
            json = sr.ReadLine();
            settings = JsonUtility.FromJson<PandaSettings>(json); 
        } else {
            Debug.Log("Could not Open the file: " + json + " for reading.");
            return;
        }
        Debug.Log("Settings loaded successfully");
    }

    public override void Ping()
    {

        byte[] buffer = Converter.CreatePackage(PackageType.PING);
        if (debugEnabled == true) Debug.Log("Send to: " + endPoint.ToString());
        sendingNetworkSocket.SendTo(buffer, endPoint);
    }

    public override void SendStatus()
    {
        byte[] buffer = Converter.CreatePackage(PackageType.STATUS);
        if (debugEnabled == true) Debug.Log("Send to: " + endPoint.ToString());
        sendingNetworkSocket.SendTo(buffer, endPoint);
    }

    public override void SendSettings()
    {
        if (sendingNetworkSocket == null)
        {
            InitUDPSocket();
        }

        byte[] buffer = Converter.CreateSettingsPackage(speed);
        sendingNetworkSocket.SendTo(buffer, endPoint);

    }

    public override void Move()
    {
        if (operatingMode == OperatingMode.JointSpace)
        {
            this.MoveJoints();
        }
        else
        {
            this.MoveToPoint();
        }
    }
    

    public void Grasp()
    {
        byte[] buffer = Converter.CreateGraspPackage(objectWidth, gripperSpeed, force, epsilon_inner, epsolon_outer);
        sendingNetworkSocket.SendTo(buffer, endPoint);
    }

    /// <summary>
    /// Grasps an object.
    /// An object is considered grasped if the distance d between the gripper fingers satisfies:
    /// $$(width - epsilon_inner) < d < (width + epsilon_outer)$$
    /// </summary>
    /// <param name="objectWidth">Size of the object to grasp. [m]</param>
    /// <param name="gripperSpeed">Closing speed. [m/s] </param>
    /// <param name="force">Grasping force in [N]</param>
    /// <param name="epsilon_inner">Maximum tolerated deviation when the actual grasped width is smaller than the commanded grasp width.</param>
    /// <param name="epsolon_outer">Maximum tolerated deviation when the actual grasped width is larger than the commanded grasp width</param>
    public void Grasp(float objectWidth, float gripperSpeed, float force, float epsilon_inner, float epsolon_outer)
    {
        Debug.Log("Grasped Object: " + objectWidth);
        this.objectWidth = objectWidth;
        this.gripperSpeed = gripperSpeed;
        this.force = force;
        this.epsilon_inner = epsilon_inner;
        this.epsolon_outer = epsolon_outer;
        Grasp();

    }

    /// <summary>
    /// releases object and completely opens fingers 
    /// </summary>
    public void Release()
    {
        byte[] buffer = Converter.CreateReleasePackage();
        sendingNetworkSocket.SendTo(buffer, endPoint);
    }

    public void OnValidate()
    {
        UpdateJoints();
    }

    public void UpdateJoints()
    {
        jointUpdate.ApplyJoints();
    }
    public void UpdateJoints(JointPose jointsPose)
    {
        Debug.Log("Joint Pose updated");
        this.jointPose = jointsPose;
        jointUpdate.ApplyJoints();
    }

    #endregion
    
    #region Network
    
 
    public void InitUDPSocket() {
        Debug.Log("Init UDP");
        
        if (debugEnabled == true) Debug.Log("Init Socket with: UDP");
        receivingNetworkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
            ProtocolType.Udp);
        var remoteEP = new IPEndPoint(IPAddress.Any, rec_port);
        receivingNetworkSocket.Bind(remoteEP); 
        
        sendingNetworkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
            ProtocolType.Udp);

        IPAddress serverAddr = IPAddress.Parse(settings.ReceiverIp);
     
        endPoint = new IPEndPoint(serverAddr, settings.ReceiverPort);
        jointUpdate = FindObjectOfType<JointUpdater>();

        FrankaPanda.instance = this;
        
       
        //udpServer = new UdpClient(rec_port);
        remoteEP = new IPEndPoint(IPAddress.Any, rec_port);
        
    }
   

    #endregion
   
    
    [System.Serializable]
    public class PandaSettings : Settings
    {
        public string PandaIp;
        public string ReceiverIp;
        public string name;
    }

    private static FrankaPanda instance;
    public static FrankaPanda GetInstance()
    {
        return instance;
    }

}


public enum CartesianPoints
{
    INITIAL,
    PICK_UP
}

public enum JointPoses
{
    INITIAL,
    FORWARD
}