﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(FrankaPanda))]
public class FrankaPandaEditor : Editor {
    
    SerializedProperty m_CartesianPose;
    SerializedProperty m_JointProp;
    SerializedProperty m_SettingsProp;
    SerializedProperty m_OperationModeProp;
    SerializedProperty m_jointUpdaterProp;
    public CartesianPoints points;
    public JointPoses poses;
    void OnEnable()
    {
        // Fetch the objects from the GameObject script to display in the inspector
        m_CartesianPose = serializedObject.FindProperty("_cartesianPose");
        m_JointProp = serializedObject.FindProperty("jointPose");
        m_SettingsProp = serializedObject.FindProperty("settings");
        
        //m_VectorProp = serializedObject.FindProperty("m_MyVector");
        m_jointUpdaterProp = serializedObject.FindProperty("jointUpdate");
    }
    

    
    // Start is called before the first frame update
    public override void OnInspectorGUI () {
        var myScript = target as FrankaPanda;
        
        //Called whenever the inspector is drawn for this object.
        //DrawDefaultInspector();
        GUILayout.BeginVertical("ObjectField");
 
        GUILayout.Label("Configuration");
        EditorGUILayout.PropertyField(m_SettingsProp, new GUIContent("Settings"));
        myScript.ConfigFileName = EditorGUILayout.TextField("Config File Path",myScript.ConfigFileName );
        GUILayout.BeginHorizontal();
        if(GUILayout.Button("Load Settings"))
            myScript.LoadSettings();
        if(GUILayout.Button("Save Settings"))
            myScript.SaveSettings();

        GUILayout.EndHorizontal();
 
        GUILayout.EndVertical ();

        //EditorGUILayout.BeginToggleGroup("Test", false);
        

        
        //EditorGUILayout.EndToggleGroup();
                
        EditorGUILayout.Separator();
        myScript.updateJoints = EditorGUILayout.Toggle("Joint Updates", myScript.updateJoints);
        myScript.moveToInitPositionOnStartup = EditorGUILayout.Toggle("Move to init on startup", myScript.moveToInitPositionOnStartup);
         
        myScript.operatingMode = (FrankaPanda.OperatingMode)EditorGUILayout.EnumPopup("Operating Mode", myScript.operatingMode);

        if (myScript.operatingMode == FrankaPanda.OperatingMode.CartesianSpace)
        {
            myScript.updateJoints = EditorGUILayout.Toggle("Joint Updates", myScript.updateJoints);
            myScript.restrictCartesianArea = EditorGUILayout.Toggle("Restrict Cartesian Area", myScript.restrictCartesianArea);
            myScript.restrictionPoint = EditorGUILayout.Vector3Field("Point", myScript.restrictionPoint);
            myScript.scale = EditorGUILayout.Vector3Field("Radius", myScript.scale);
            if (GUILayout.Button("Apply"))
            {
                myScript.SetRestrictionArea();
            }
            if (GUILayout.Button("Remove"))
            {
                myScript.RemoveRestrictionArea();
            }
            
            
            EditorGUILayout.BeginHorizontal();
            points = (CartesianPoints)EditorGUILayout.EnumPopup("Points", points);
            if (GUILayout.Button("Choose Point"))
            {
                if (points == CartesianPoints.PICK_UP)
                {
                    myScript._cartesianPose.position = new Vector3(0, -0.03f, 0.4f);
                } else if (points == CartesianPoints.INITIAL)
                {
                    myScript._cartesianPose.position = new Vector3(0, 0.5f, 0.3f);
                }
            }
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.PropertyField(m_CartesianPose, new GUIContent("End Effector Position"));

        }



        if (myScript.operatingMode == FrankaPanda.OperatingMode.JointSpace)
        {
            EditorGUILayout.BeginHorizontal();
            poses = (JointPoses)EditorGUILayout.EnumPopup("Joint Poses", poses);
            if (GUILayout.Button("Choose Joint Pose"))
            {
                if (poses == JointPoses.INITIAL)
                {
                    myScript.jointPose = JointPose.initialPose;
                } else if (poses == JointPoses.FORWARD)
                {
                    myScript.jointPose.joint_0 = 0;
                    myScript.jointPose.joint_1 = 1.39;
                    myScript.jointPose.joint_2 = 0;
                    myScript.jointPose.joint_3 = -0.36;
                    myScript.jointPose.joint_4 = 0;
                    myScript.jointPose.joint_5 = 1.71;
                    myScript.jointPose.joint_6 = Math.PI / 4;
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.PropertyField(m_JointProp, new GUIContent("Joint Position"));
            
        } else if (myScript.operatingMode == FrankaPanda.OperatingMode.JointRealTime)
        {
            
            EditorGUILayout.PropertyField(m_JointProp, new GUIContent("Joint Position"));
            myScript.realtimeRefreshRate = (int)EditorGUILayout.IntField("Realtime Refresh Rate in ms", myScript.realtimeRefreshRate);
            
            if (GUILayout.Button("Start Realtime"))
            {
                myScript.StartRealtimeJointMotion();
            }

            if (GUILayout.Button("Stop Realtime"))
            {
                myScript.StopRealtimeJointMotion();
            }
        }




        // Apply changes to the serializedProperty - always do this at the end of OnInspectorGUI.
                
            //if(GUILayout.Button("Move Gripper"))
              //  Debug.Log("Do something");
            //myScript.MoveGripper();
            if (myScript.operatingMode != FrankaPanda.OperatingMode.JointRealTime)
            {
                GUILayout.BeginHorizontal();
                if(GUILayout.Button("Move Robot"))
                    myScript.Move();
                if (GUILayout.Button("Init Pose"))
                {
                    if (myScript.operatingMode == FrankaPanda.OperatingMode.JointSpace)
                    {
                
                        myScript.jointPose = JointPose.initialPose; 
                    }
                    else
                    {
                        myScript._cartesianPose = CartesianPose.initialPose; 
                    }

                    myScript.OnValidate();
                }
        
                GUILayout.EndHorizontal();
            }

          
        
        

        GUILayout.BeginVertical("HelpBox");
        GUILayout.Label("Configuration");
        
        myScript.objectWidth = EditorGUILayout.DoubleField("Object Width [m]", myScript.objectWidth);
        myScript.gripperSpeed = EditorGUILayout.DoubleField("Gripper Speed [m/s]", myScript.gripperSpeed);
        myScript.force = EditorGUILayout.DoubleField("Gripper Force [N]", myScript.force);
        myScript.epsilon_inner = EditorGUILayout.DoubleField("Inner Epsilon [m]", myScript.epsilon_inner);
        myScript.epsolon_outer = EditorGUILayout.DoubleField("Outer Epsilon [m]", myScript.epsolon_outer);

        GUILayout.BeginHorizontal();
        if(GUILayout.Button("Grasp"))
            myScript.Grasp();
        if(GUILayout.Button("Release"))
            myScript.Release();
        GUILayout.EndHorizontal();

          
        
       
        GUILayout.EndVertical ();
        
        
        EditorGUILayout.PropertyField(m_jointUpdaterProp, new GUIContent("Joint Updater"));

        
        serializedObject.ApplyModifiedProperties();
        
        
        //add everthing the button would do.
    }
}

public enum CartesianPoints
{
    INITIAL,
    PICK_UP
}

public enum JointPoses
{
    INITIAL,
    FORWARD
}
