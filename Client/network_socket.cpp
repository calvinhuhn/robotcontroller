//
// Created by calvin on 17.08.20.
//

#include "network_socket.h"

void network_socket::send(std::array<double, 7> current_q) {
    Poco::Net::SocketAddress sa("172.16.1.205", 53301);
    Poco::Net::DatagramSocket dgs;
    dgs.connect(sa);

    unsigned char buffer[sizeof(double) * 7];
    //Pointer now shows to buffer
    unsigned char *bufferPtr = buffer;



    for (int i = 0; i < 7; ++i) {
        //*bufferPtr = (unsigned char)current_q[i];
        memcpy(bufferPtr + i * sizeof(double), &current_q[i], sizeof(double));

    }

    dgs.sendBytes(buffer,sizeof(double) * 7 );
}
