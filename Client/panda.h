//
// Created by calvin on 17.08.20.
//

#ifndef CLIENT_PANDA_H
#define CLIENT_PANDA_H



//#include <Eigen/Dense>
#include <math.h>

#include <franka/control_types.h>
#include <franka/duration.h>
#include <franka/robot.h>
#include <franka/robot_state.h>
#include <Eigen/Core>
#include <array>
#include <cmath>
#include <functional>
#include <iostream>
#include <Eigen/Dense>

#include <franka/exception.h>
#include <franka/model.h>


#include "network_socket.h"
#include <franka/robot.h>
#include <franka/gripper.h>
#include <franka/exception.h>

#include "examples_common.h"


class panda {
public:
    franka::Robot robot = franka::Robot("172.16.3.147");
    franka::Gripper gripper = franka::Gripper("172.16.3.147");


    std::array<double, 7> current_q = {0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4};

    std::array<double, 3> d_position = {0, 0, 0};
    std::array<double, 3> d_orientation = {0, 0,0};
    int test;
    void start();
    void receive_update();
    void send_update();
    double speed = 0.5;
    network_socket sender;
    void move_to_init_pose(double speed);
    const double deg_to_rad = M_PI / 180;
    franka::RobotState state;
    //Eigen::Vector3d position_d;
    //Eigen::Vector3d rotation_d;
    double current_gripper_width = 0.0;
    double gripper_force = 0.0;


    Poco::Net::DatagramSocket dgs;



    std::array<double, 3> get_endeffector_position();

private:

    void move_to_cartesian_pose(char *ptr);

    void move_to_joint_pose(char *ptr);

    void gripper_pickup(char *ptr);

    void gripper_release();

    void execute_action(unsigned char signal_bytes, char *ptr);

    std::array<double, 7> get_joints();

    void get_joints(char *ptr);

    void wait_for_packages();



    void get_cartesian_pose(char *ptr);

    void move_to_cartesian_pose(char *ptr, double movement_time);

    void cartesian_impendace();

    void cartesian_impendace(double translational_stiffness, double rotational_stiffness);

    void move_to_cartesian_orientation(char *ptr, double movement_time);





    void local_rotation(char *ptr);



    double* copy_orientation(char *ptr);


    void send_position(std::array<double, 3> position);

    void init_streaming_network(std::string ip);

    void init_streaming_network(std::string ip, int port);

    std::string get_ip();

    std::string get_ip(char *ptr);

    int get_port(char *ptr);

    std::array<double, 3> get_endeffector_orientation_euler();

    std::array<double, 4> get_endeffector_orientation_quaternions();

    void send_orientation(std::array<double, 4> quaternions);

    void send_orientation(std::array<double, 3> euler);

    void send_robot_q();
};

struct cartesian_pose{
    double position_x;

    double getPositionX() const {
        return position_x;
    }

    void setPositionX(double positionX) {
        position_x = positionX;
    }

    double getPositionY() const {
        return position_y;
    }

    void setPositionY(double positionY) {
        position_y = positionY;
    }

    double getPositionZ() const {
        return position_z;
    }

    void setPositionZ(double positionZ) {
        position_z = positionZ;
    }

    double getOrientationX() const {
        return orientation_x;
    }

    void setOrientationX(double orientationX) {
        orientation_x = orientationX;
    }

    double getOrientationY() const {
        return orientation_y;
    }

    void setOrientationY(double orientationY) {
        orientation_y = orientationY;
    }

    double getOrientationZ() const {
        return orientation_z;
    }

    void setOrientationZ(double orientationZ) {
        orientation_z = orientationZ;
    }

public:
    double position_y;
    double position_z;
    double orientation_x;
    double orientation_y;
    double orientation_z;

    cartesian_pose(double p_x, double p_y, double p_z, double o_x, double o_y, double o_z){
        this->position_x = p_x;
        this->position_y = p_y;
        this->position_z = p_z;
        this->orientation_x = o_x;
        this->orientation_y = o_y;
        this->orientation_z = o_z;
    }

};

#endif //CLIENT_PANDA_H
