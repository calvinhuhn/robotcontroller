//
// Created by calvin on 17.08.20.
//


#include <Poco/Net/SocketAddress.h>
#include <Poco/Net/DatagramSocket.h>
#include <iostream>
#include "panda.h"
#include "Generator.h"
#include <array>



void panda::start() {
    std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};

    //this->move_to_init_pose(0.5);
    get_endeffector_position();
    wait_for_packages();
    //receive_update();

}

void panda::get_cartesian_pose(char* ptr){
    memcpy(&d_position[0], ptr + 1, sizeof(d_position[0]));
    memcpy(&d_position[1], ptr + 9, sizeof(d_position[1]));
    memcpy(&d_position[2], ptr + 17, sizeof(d_position[2]));
    memcpy(&d_orientation[0], ptr + 25, sizeof(d_orientation[0]));
    memcpy(&d_orientation[1], ptr + 33, sizeof(d_orientation[1]));
    memcpy(&d_orientation[2], ptr + 41, sizeof(d_orientation[2]));
    memcpy(&speed, ptr + 49, sizeof(speed));


}

double* panda::copy_orientation(char* ptr){
    double out [4];
    double *o = out;

    memcpy(&out[0], ptr + 1, sizeof(out[0])); //roll
    memcpy(&out[1], ptr + 9, sizeof(out[0])); //pitch
    memcpy(&out[2], ptr + 17, sizeof(out[0])); //yaw
    memcpy(&out[3], ptr + 25, sizeof(out[0])); //movement_time

    return o;
    //return out;

}

void panda::local_rotation(char* ptr){


    double roll_vel = 0, pitch_vel = 0, yaw_vel = 0;
    double* tuple = copy_orientation(ptr);
    double roll = tuple[0];
    double pitch = tuple[1];
    double yaw = tuple[2];
    double movement_time = tuple[3];


    std::cout << "Starting Rotation: {roll: " << roll << ", pitch: " << pitch << ", yaw: " << yaw << "} movement time: " << movement_time << std::endl;


    double time = 0.0;
    double frac = 0.0;
    std::array<double, 6> vel_out = {0,0,0,0,0,0};
    double curve = 0.0;

    std::cout << "" << std::endl;
    robot.control([this, &curve, &vel_out, &movement_time, &pitch, &yaw, &roll, &roll_vel, &pitch_vel, &yaw_vel, &frac, &time](
            const franka::RobotState &robot_state,
            franka::Duration period) -> franka::CartesianVelocities {

        time += period.toSec();
        if (time == 0.0) {
            return vel_out;
        }

        if(time > movement_time){
            std::cout << "Rotation finished" << std::endl << "----------" << std::endl;
            return franka::MotionFinished(std::array<double, 6>{0,0,0,0,0,0});
        }

        frac = time / movement_time;
        curve = 30 * (frac * frac) - 60 * (frac * frac * frac) + 30 * (frac * frac * frac * frac);
        pitch_vel   = (1 / movement_time) *  (pitch)   * curve;
        roll_vel    = (1 / movement_time) *  (roll)    * curve;
        yaw_vel     = (1 / movement_time) *  (yaw)     * curve;

        vel_out[4] = pitch_vel;
        vel_out[3] = roll_vel;
        vel_out[5] = yaw_vel;
        return vel_out;
    });

}

void panda::move_to_cartesian_orientation(char* ptr, double movement_time){
    get_cartesian_pose(ptr);

    Eigen::Affine3d initial_transform(Eigen::Matrix4d::Map(state.O_T_EE.data()));
    Eigen::Vector3d position_d(initial_transform.translation());
    Eigen::Quaterniond orientation_d(initial_transform.linear());


    std::function<franka::CartesianPose(const franka::RobotState&, franka::Duration)>
            cartesian_pose_callback = [&](const franka::RobotState& robot_state, franka::Duration) -> franka::CartesianPose {

        Eigen::Affine3d transform(Eigen::Matrix4d::Map(state.O_T_EE.data()));
        Eigen::Vector3d position(transform.translation());
        Eigen::Quaterniond orientation(transform.linear());

        std::cout << transform.linear() << std::endl;

        Eigen::Matrix<double, 6, 1> error;
        error.head(3) << position - position_d;

        Eigen::Quaterniond error_quaternion(orientation.inverse() * orientation_d);
        error.tail(3) << error_quaternion.x(), error_quaternion.y(), error_quaternion.z();
        // Transform to base frame
        error.tail(3) << -transform.linear() * error.tail(3);


    };

}

void panda::move_to_cartesian_pose(char* ptr, double movement_time){

    //copy pose from byte array
    get_cartesian_pose(ptr);
    std::array<double, 16> initial_pose;
    std::array<double, 16> new_pose = initial_pose;
    double time = 0.0;
    double frac = 0.0;
    std::cout << "Cartesian Point Motion: {" << d_position[0] << ", " << d_position[1] << ", " << d_position[2] << "}" << " with movement duration: " << speed << " sec"  << std::endl;

    try {
        robot.control([this, &movement_time, &frac, &new_pose, &time, &initial_pose](
                const franka::RobotState &robot_state,
                franka::Duration period) -> franka::CartesianPose {
            time += period.toSec();
            if (time == 0.0) {
                initial_pose = robot_state.O_T_EE_c;
                new_pose = robot_state.O_T_EE_c;
                return new_pose;
            }
            frac = time / movement_time; //period.toSec();
            new_pose[12] = initial_pose[12] + (this->d_position[0] - initial_pose[12]) * (10.0 * (frac*frac*frac) - 15 * (frac*frac*frac*frac) + 6.0 * (frac*frac*frac*frac*frac));
            new_pose[13] = initial_pose[13] + (this->d_position[1] - initial_pose[13]) * (10.0 * (frac*frac*frac) - 15 * (frac*frac*frac*frac) + 6.0 * (frac*frac*frac*frac*frac));
            new_pose[14] = initial_pose[14] + (this->d_position[2] - initial_pose[14]) * (10.0 * (frac*frac*frac) - 15 * (frac*frac*frac*frac) + 6.0 * (frac*frac*frac*frac*frac));

            if (time >= movement_time) {
                std::cout << "Finished motion!" << std::endl;
                return franka::MotionFinished(new_pose);
            }

            return new_pose;
        });
    } catch (const franka::Exception& e) {
        std::cout << e.what() << std::endl;
        std::cout << "Running error recovery" << std::endl;
        robot.automaticErrorRecovery();
    }

    std::cout << std::endl << "-------------------" << std::endl;
}

void panda::cartesian_impendace(double translational_stiffness, double rotational_stiffness){
    // Compliance parameters

    Eigen::MatrixXd stiffness(6, 6), damping(6, 6);
    stiffness.setZero();
    stiffness.topLeftCorner(3, 3) << translational_stiffness * Eigen::MatrixXd::Identity(3, 3);
    stiffness.bottomRightCorner(3, 3) << rotational_stiffness * Eigen::MatrixXd::Identity(3, 3);
    damping.setZero();
    damping.topLeftCorner(3, 3) << 2.0 * sqrt(translational_stiffness) *
                                   Eigen::MatrixXd::Identity(3, 3);
    damping.bottomRightCorner(3, 3) << 2.0 * sqrt(rotational_stiffness) *
                                       Eigen::MatrixXd::Identity(3, 3);
    try {
        // connect to robot
        setDefaultBehavior(robot);
        // load the kinematics and dynamics model
        franka::Model model = robot.loadModel();
        franka::RobotState initial_state = robot.readOnce();
        // equilibrium point is the initial position
        Eigen::Affine3d initial_transform(Eigen::Matrix4d::Map(initial_state.O_T_EE.data()));
        Eigen::Vector3d position_d(initial_transform.translation());
        Eigen::Quaterniond orientation_d(initial_transform.linear());
        // set collision behavior
        robot.setCollisionBehavior({{100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0}},
                                   {{100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0}},
                                   {{100.0, 100.0, 100.0, 100.0, 100.0, 100.0}},
                                   {{100.0, 100.0, 100.0, 100.0, 100.0, 100.0}});
        // define callback for the torque control loop
        std::function<franka::Torques(const franka::RobotState&, franka::Duration)>
                impedance_control_callback = [&](const franka::RobotState& robot_state,
                                                 franka::Duration /*duration*/) -> franka::Torques {
            // get state variables
            std::array<double, 7> coriolis_array = model.coriolis(robot_state);
            std::array<double, 42> jacobian_array =
                    model.zeroJacobian(franka::Frame::kEndEffector, robot_state);
            // convert to Eigen
            Eigen::Map<const Eigen::Matrix<double, 7, 1>> coriolis(coriolis_array.data());
            Eigen::Map<const Eigen::Matrix<double, 6, 7>> jacobian(jacobian_array.data());
            Eigen::Map<const Eigen::Matrix<double, 7, 1>> q(robot_state.q.data());
            Eigen::Map<const Eigen::Matrix<double, 7, 1>> dq(robot_state.dq.data());
            Eigen::Affine3d transform(Eigen::Matrix4d::Map(robot_state.O_T_EE.data()));
            Eigen::Vector3d position(transform.translation());
            Eigen::Quaterniond orientation(transform.linear());
            // compute error to desired equilibrium pose
            // position error
            Eigen::Matrix<double, 6, 1> error;
            error.head(3) << position - position_d;
            // orientation error
            // "difference" quaternion
            if (orientation_d.coeffs().dot(orientation.coeffs()) < 0.0) {
                orientation.coeffs() << -orientation.coeffs();
            }
            // "difference" quaternion
            Eigen::Quaterniond error_quaternion(orientation.inverse() * orientation_d);
            error.tail(3) << error_quaternion.x(), error_quaternion.y(), error_quaternion.z();
            // Transform to base frame
            error.tail(3) << -transform.linear() * error.tail(3);
            // compute control
            Eigen::VectorXd tau_task(7), tau_d(7);
            // Spring damper system with damping ratio=1
            tau_task << jacobian.transpose() * (-stiffness * error - damping * (jacobian * dq));
            tau_d << tau_task + coriolis;
            std::array<double, 7> tau_d_array{};
            Eigen::VectorXd::Map(&tau_d_array[0], 7) = tau_d;
            return tau_d_array;
        };
        // start real-time control loop
        std::cout << "WARNING: Collision thresholds are set to high values. "
                  << "Make sure you have the user stop at hand!" << std::endl
                  << "After starting try to push the robot and see how it reacts." << std::endl
                  << "Press Enter to continue..." << std::endl;
        std::cin.ignore();
        robot.control(impedance_control_callback);
    } catch (const franka::Exception& ex) {
        // print exception
        std::cout << ex.what() << std::endl;
        std::cout << "Running error recovery" << std::endl;
        robot.automaticErrorRecovery();}
}


/// \brief moves robot to joint pose
/// \param ptr
void panda::move_to_joint_pose(char* ptr){

    get_joints(ptr);
    Generator GeneratorInit(0.5, current_q);
    std::cout << "Moving joints: {" << current_q[0] << ", " << current_q[1] << ", "<< current_q[2] << ", "<< current_q[3] << ", "<< current_q[4] << ", "<< current_q[5] << ", " << current_q[6] << "} width speed: " << speed << std::endl;
    try {
        robot.control(GeneratorInit);
    } catch (franka::Exception& e) {
        std::cout << e.what() << std::endl;
        std::cout << "Running error recovery" << std::endl;
        robot.automaticErrorRecovery();
    }
    std::cout << "Movement finished!" << std::endl << "----------" << std::endl;

}
void panda::get_joints(char* ptr){
    memcpy(&current_q[0], ptr + 1, sizeof(current_q[0]));
    memcpy(&current_q[1], ptr + 9, 8);
    memcpy(&current_q[2], ptr + 17, 8);
    memcpy(&current_q[3], ptr + 25, 8);
    memcpy(&current_q[4], ptr + 33, 8);
    memcpy(&current_q[5], ptr + 41, 8);
    memcpy(&current_q[6], ptr + 49, 8);
    memcpy(&speed, ptr + 57, speed);
}



void panda::gripper_pickup(char* ptr){
    double object_width = 0.0;
    double speed = 0.0;
    double force = 0.0;
    double inner_epsilon = 0.0;
    double outer_epsilon = 0.0;

    memcpy(&object_width, ptr, sizeof(object_width));
    memcpy(&speed, ptr + 9, 8);
    memcpy(&force, ptr + 17, 8);
    memcpy(&inner_epsilon, ptr + 25, 8);
    memcpy(&outer_epsilon, ptr + 33, 8);
    gripper.grasp(object_width, speed, force, inner_epsilon, outer_epsilon);
}

/// \brief releases objects
void panda::gripper_release(){
    gripper.move(0.08f, 0.5f);
}


std::array<double, 3> panda::get_endeffector_position(){
    const franka::RobotState state = robot.readOnce();
    std::array<double, 3> position = {state.O_T_EE[12], state.O_T_EE[13], state.O_T_EE[14]};
    return position;
}

std::array<double, 3> panda::get_endeffector_orientation_euler(){
    const franka::RobotState state = robot.readOnce();
    //copy to Matrix

    //calc euler from Matrix

}

std::array<double, 4> panda::get_endeffector_orientation_quaternions(){
    //todo: no new array
    std::array<double, 4> quaternions{};
    const franka::RobotState state = robot.readOnce();
    //copy to Matrix
    Eigen::Affine3d initial_transform(Eigen::Matrix4d::Map(state.O_T_EE.data()));
    auto rotation =  initial_transform.rotation();
    //quaternions[0] = rotation.x();
    //quaternions[1] = rotation.y();
    //quaternions[2] = rotation.z();
    //quaternions[3] = rotation.w();
    return quaternions;
}

void panda::init_streaming_network(std::string ip, int port){
    Poco::Net::SocketAddress sa(ip, port);
    std::cout << "connecting to unity with: {ip: "  <<  ip << ", port: " << port <<  "}"<< std::endl;
    dgs.connect(sa);
    char ack[1];
    ack[0] = 10;
    std::cout << "successful: sending acknowledgement" << std::endl;
    std::cout << std::endl << "-------------------" << std::endl;
    dgs.sendBytes(ack, 1);
}

std::string panda::get_ip(char* ptr){
    std::stringstream ss;
    ss << (int)(unsigned char)ptr[1] << "." << (int)(unsigned char)ptr[2] <<  "." << (int)(unsigned char)ptr[3] << "." << (int)(unsigned char)ptr[4];
    std::string s = ss.str();
    return s;
}

int panda::get_port(char* ptr){
    int port = 0;
    memcpy(&port, ptr + 5, 4);
    return port;
}
void panda::send_orientation(std::array<double, 3> euler){
    double* array_ptr = euler.data();
    char buffer[25];
    char* buffer_ptr = buffer;
    memcpy(buffer_ptr + 0, &array_ptr[0], 8);
    memcpy(buffer_ptr + 8, &array_ptr[1], 8);
    memcpy(buffer_ptr + 16, &array_ptr[2], 8);
    dgs.sendBytes(buffer, 25);
}

void panda::send_orientation(std::array<double, 4> quaternions){
    double* array_ptr = quaternions.data();
    char buffer[32];
    char* buffer_ptr = buffer;
    memcpy(buffer_ptr + 0, &array_ptr[0], 8);
    memcpy(buffer_ptr + 8, &array_ptr[1], 8);
    memcpy(buffer_ptr + 16, &array_ptr[2], 8);
    memcpy(buffer_ptr + 24, &array_ptr[3], 8);
    dgs.sendBytes(buffer, 32);
}

void panda::send_robot_q(){
    franka::RobotState initial_state = robot.readOnce();
    std::array<double, 7> q = initial_state.q;

    double* array_ptr = q.data();
    char buffer[sizeof(double)*7];
    char* buffer_ptr = buffer;

    for (int i = 0; i < 7; ++i) {
        memcpy(buffer_ptr + i * sizeof(double), &array_ptr[i], sizeof(double));
    }
    dgs.sendBytes(buffer, sizeof(double)*7);
}

void panda::send_position(std::array<double, 3> position){
    double* array_ptr = position.data();
    char buffer[25];
    char* buffer_ptr = buffer;
    memcpy(buffer_ptr + 16, &array_ptr[0], 8);
    memcpy(buffer_ptr + 0, &array_ptr[1], 8);
    memcpy(buffer_ptr + 8, &array_ptr[2], 8);
    dgs.sendBytes(buffer, 25);
}

//void panda::send_orientation(double )

void panda::execute_action(unsigned char action_code,  char* ptr){
    switch (action_code) {
        case 1:
            move_to_cartesian_pose(ptr, 2.0);
            //move_to_cartesian_orientation(ptr, 2.0);
            //local_rotation(ptr, 2.0);
            break;
        case 2:
            move_to_joint_pose(ptr);
            break;
        case 3:
            std::cout << "realtime cartesian motion not implemented" << std::endl;
            break;
        case 4:
            std::cout << "realtime cartesian motion not implemented" << std::endl;
            break;
        case 5:
            std::cout << "cartesian trajectory not implemented" << std::endl;
            break;
        case 6:
            std::cout << "joint trajectory not implemented" << std::endl;
            break;
        case 7:
            gripper_pickup(ptr);
            break;
        case 8:
            gripper_release();
            break;
        case 9:
            std::cout << "move gripper motion not implemented" << std::endl;
            break;
        case 10:
            init_streaming_network(get_ip(ptr), get_port(ptr));
            break;
        case 11:
            std::cout << "shut down not implemented" << std::endl;
            break;
        case 12:
            std::cout << "break release not implemented" << std::endl;
            break;
        case 13:
            std::cout << "break activation not implemented" << std::endl;
            break;
        case 14:
            cartesian_impendace(150, 10);
            break;
        case 18:
            local_rotation(ptr);
            break;
        case 19:
            send_position(get_endeffector_position());
            break;
        case 20:
            //send rotation euler
            send_orientation(get_endeffector_orientation_euler());
            break;
        case 21:
            //send rotation quaternions
            send_orientation(get_endeffector_orientation_quaternions());
            break;
        case 22:
            //send joint q
            send_robot_q();
            break;
    }
}



void panda::wait_for_packages(){
    Poco::Net::SocketAddress socketAddress(Poco::Net::IPAddress(), 53301);
    //todo send socket adress from unity
    Poco::Net::SocketAddress receiverAddress("172.16.5.237", 53301);
    Poco::Net::DatagramSocket datagramSocket(socketAddress);

    char buffer[64];
    char* ptr = buffer;
    unsigned char action_code = 1;

    //region update loop
    Poco::Net::SocketAddress sender;

    for(;;) {

        datagramSocket.receiveFrom(buffer, sizeof(buffer) - 1, sender);
        memcpy(&action_code, ptr, 1);
        try {
            execute_action(action_code, ptr);
        } catch(...) {
            std::cout << "unhandled exception. closing sockets and shutting down" << std::endl;
            action_code = 0;

        }




        if (action_code == 0){

            std::cout << "Terminated: action code 0" << std::endl;
            dgs.close();
            return;
        }
    }
}

void panda::send_update() {
// sender.send(current_q);
}

void panda::move_to_init_pose(double speed) {
    std::array<double, 7> init = {0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4};
    //create motion generator
    Generator GeneratorInit(speed, init);

    robot.control(GeneratorInit);

}





