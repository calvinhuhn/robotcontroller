//
// Created by calvin on 17.08.20.
//

#ifndef CLIENT_NETWORK_SOCKET_H
#define CLIENT_NETWORK_SOCKET_H
#include <array>
#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"


class network_socket {


    Poco::Net::SocketAddress sa;
    Poco::Net::DatagramSocket dgs;

public:
    void send(std::array<double, 7> current_q);
    network_socket(){
        this->sa =  Poco::Net::SocketAddress("172.16.1.205", 53301);
        this->dgs = Poco::Net::DatagramSocket();
        this->dgs.connect(sa);
    }

};


#endif //CLIENT_NETWORK_SOCKET_H
