//
// Created by calvin on 17.08.20.
//

#ifndef CLIENT_NETWORK_RECEIVER_H
#define CLIENT_NETWORK_RECEIVER_H
#include <Poco/Net/DatagramSocket.h>
#include <Poco/Net/SocketAddress.h>
#include <Poco/Net/NetException.h>
#include <iostream>
#include <Eigen/Dense>

#include <thread>



class network_receiver {
public:

/*
    void operator()(Eigen::Vector3d &position) {
        Poco::Net::SocketAddress sa(Poco::Net::IPAddress(), 53301);
        Poco::Net::DatagramSocket dgs(sa);
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        char buffer[25];
        char* ptr = buffer;
        int i = 0;
        for(;;) {
            Poco::Net::SocketAddress sender;
            std::cout << "Waiting for bytes" << std::endl;
            //n = length
            int n = dgs.receiveFrom(buffer, sizeof(buffer) - 1, sender);
            //buffer[n] = '\0';
            std::cout << "Position updated" << std::endl;
            memcpy(&x, ptr, sizeof(double));
            memcpy(&y, ptr + 8, sizeof(double));
            memcpy(&z, ptr + 16, sizeof(double));
            position.x() = x;
            position.y() = y;
            position.z() = z;
    }
*/
    std::thread t1;


    void end_thread() {

    }
    void start(Eigen::Vector3d &position, Eigen::Vector3d &rotation) {

        Poco::Net::SocketAddress sa(Poco::Net::IPAddress(), 53301);
        Poco::Net::DatagramSocket dgs(sa);
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;

        double r_x = 0.0;
        double r_y = 0.0;
        double r_z = 0.0;
        char buffer[49];
        char* ptr = buffer;
        int i = 0;
        for(;;) {
            Poco::Net::SocketAddress sender;
            std::cout << "Waiting for bytes" << std::endl;
            //n = length
            int n = dgs.receiveFrom(buffer, sizeof(buffer) - 1, sender);
            //buffer[n] = '\0';
            std::cout << "Position updated" << std::endl;
            memcpy(&x, ptr, sizeof(double));
            memcpy(&y, ptr + 8, sizeof(double));
            memcpy(&z, ptr + 16, sizeof(double));
            memcpy(&r_x, ptr + 24, sizeof(double));
            memcpy(&r_y, ptr + 32, sizeof(double));
            memcpy(&r_z, ptr + 40, sizeof(double));
            position.x() = x;
            position.y() = y;
            position.z() = z;

            rotation.x() = r_x;
            rotation.y() = r_y;
            rotation.z() = r_z;

        }




    };


#endif //CLIENT_NETWORK_RECEIVER_H
